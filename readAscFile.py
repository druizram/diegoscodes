import pandas as pd
import numpy as np #used for calculating the norm



def read_data(file_path, column_names):
    """Read data from a CSV file and return a DataFrame."""
    data = pd.read_csv(file_path, comment='#', delim_whitespace=True, encoding='ISO-8859-1', names=column_names)
    return data

# Ruta al archivo .asc
file_path = 'prueba.asc'

# Definir los nombres de las columnas
column_names = ['date', 'u', 'v', 'w', 'temperature']

# Leer el archivo .asc # Encoding needed for time stamp format
data = pd.read_csv(file_path, comment='#',  delim_whitespace=True, encoding='ISO-8859-1', names = column_names)

# Show the DataFrame for debugging purposes
print("This is a part of the time series")
print(data)


# Calculate the mean of the whole time serie
mean_vector = data[['u','v','w']].mean()

# Calculate the norm of the mean
norm_mean_vector = np.sqrt(mean_vector['u']**2 + mean_vector['v']**2 + mean_vector['w']**2)

#CALCULATE THE NORM OF THE FLUCTUATION
#Calculate the fluctuation as the difference of each (u,v,w) and the mean of (u,v,w) through the whole time serie
data['u_fluctuation'] = data['u'] - mean_vector['u']
data['v_fluctuation'] = data['v'] - mean_vector['v']
data['w_fluctuation'] = data['w'] - mean_vector['w']
#Now we calculate the norm of the fluctuation at every time
data['norm_fluctuation'] = np.sqrt(data['u_fluctuation']**2 + data['v_fluctuation']**2 + data['w_fluctuation']**2)



#CALCULATE THE FLUCTUATION OF THE NORM
#Calculate the norm at every measurement
data['norm'] = np.sqrt(data['u']**2 + data['v']**2 + data['w']**2).round(2)
#Calculate the fluctuation of the norm as the difference of norm in every measurement and the norm of the mean
data['fluctuation_norm'] = data['norm'] - norm_mean_vector
#Now we show the value of the norm of the fluctuation and the fluctuation of the norm
print("This is the norm of the fluctuation")
print(data['norm_fluctuation'])
print("This if the fluctuation of the norm")
print(data['fluctuation_norm'])



#CALCULATE THE DIRECTION OF THE MEAN FLOW ITS MEAN VELOCITY, THE ANGLE OF
print("This is the mean vector")
print(mean_vector)
# Unit vectors for the Cartesian axes
unit_vector_x = np.array([1, 0, 0])
unit_vector_y = np.array([0, 1, 0])
unit_vector_z = np.array([0, 0, 1])

# Calculate angles to each axis using the dot product in raci
angle_to_x = np.degrees(np.arccos(np.dot(mean_vector, unit_vector_x) / (np.linalg.norm(mean_vector) * np.linalg.norm(unit_vector_x))))
angle_to_y = np.degrees(np.arccos(np.dot(mean_vector, unit_vector_y) / (np.linalg.norm(mean_vector) * np.linalg.norm(unit_vector_y))))
angle_to_z = np.degrees(np.arccos(np.dot(mean_vector, unit_vector_z) / (np.linalg.norm(mean_vector) * np.linalg.norm(unit_vector_z))))

print(angle_to_x, angle_to_y, angle_to_z)





#use miniconda, not anaconda
#instal rpm packages
# install latex packages


#Pending:
#Write a function that returns the norm as a pandas dataframe to avoid code duplication
#
