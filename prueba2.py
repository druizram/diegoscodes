import numpy as np

# Supongamos que tienes una lista de arreglos
lista_arreglos = [np.array([1, 2, 3]),
                  np.array([4, 5, 6]),
                  np.array([7, 8, 9])]

# Índice específico
indice = 1  # Por ejemplo, índice 1

# Extraer el valor en el índice específico para cada arreglo
valores_en_indice = [arr[indice] for arr in lista_arreglos]

# Calcular el promedio de los valores extraídos
promedio_valores = np.mean(valores_en_indice)

# Imprimir los resultados
print(f"Valores en el índice {indice} para cada arreglo: {valores_en_indice}")
print(f"Promedio de los valores en el índice {indice}: {promedio_valores}")
