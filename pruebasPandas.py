import pandas as pd

# Supongamos que df es tu DataFrame
data = {'Columna1': [1, 2, 3, 4, 5, 6, 7, 8, 9]}
df = pd.DataFrame(data)

# Especifica el tamaño de la ventana
tamano_ventana = 3

# Crea una ventana deslizante
ventana_deslizante = df['Columna1'].rolling(window=tamano_ventana)

# Define dos funciones personalizadas
def suma_ventana(ventana):
    return ventana.sum()

def promedio_ventana(ventana):
    return ventana.mean()

# Aplica las funciones a la ventana deslizante utilizando el método apply
resultado_suma = ventana_deslizante.apply(suma_ventana)
resultado_promedio = ventana_deslizante.apply(promedio_ventana)

# Agrega los resultados de vuelta al DataFrame original
df['Suma_Ventana'] = resultado_suma
df['Promedio_Ventana'] = resultado_promedio

# Visualiza el DataFrame resultante
print(df)
