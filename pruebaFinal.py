import pandas as pd
import numpy as np


def read_data(file_path, column_names):
    """Read data from a .asc file and return a Pandas DataFrame."""
    data = pd.read_csv(file_path, comment='#', delim_whitespace=True, encoding='ISO-8859-1', names=column_names)
    return data


def calculate_mean_velocity(data):
    return (data[['u', 'v', 'w']].mean())

def calculate_norm(vector):
    return (np.linalg.norm(vector))

def calculate_norm_fluctuation(data, mean_velocity):
    data['u_fluctuation'] = data['u'] - mean_velocity['u']
    data['v_fluctuation'] = data['v'] - mean_velocity['v']
    data['w_fluctuation'] = data['w'] - mean_velocity['w']
    #Now we calculate the norm of the fluctuation at every time
    data['norm_fluctuation'] = np.sqrt(data['u_fluctuation']**2 + data['v_fluctuation']**2 + data['w_fluctuation']**2)

def calculate_fluctuation_norm(data, norm_mean_velocity):
    data['norm'] = np.sqrt(data['u']**2 + data['v']**2 + data['w']**2).round(2)
    #Calculate the fluctuation of the norm as the difference of norm in every measurement and the norm of the mean
    data['fluctuation_norm'] = data['norm'] - norm_mean_velocity

def calculate_angles(vector, vector_norm):
    unit_vector_x = np.array([1, 0, 0])
    unit_vector_y = np.array([0, 1, 0])
    unit_vector_z = np.array([0, 0, 1])

    # Calculate angles to each axis using the dot product
    angle_to_x = np.degrees(np.arccos(np.dot(vector, unit_vector_x) / (vector_norm* np.linalg.norm(unit_vector_x))))
    angle_to_y = np.degrees(np.arccos(np.dot(vector, unit_vector_y) / (vector_norm* np.linalg.norm(unit_vector_y))))
    angle_to_z = np.degrees(np.arccos(np.dot(vector, unit_vector_z) / (vector_norm * np.linalg.norm(unit_vector_z))))

    return (angle_to_x, angle_to_y, angle_to_z)



def main():
    file_path = 'prueba.asc'
    column_names = ['date', 'u', 'v', 'w', 'temperature']

    data = read_data(file_path, column_names)

    """
    start_idx = 2  # Define your start index
    end_idx = 5    # Define your end index
    sliced_data = data.iloc[start_idx, end_idx]
    """

    sliced_data = data

    # Calculate mean
    mean_velocity = calculate_mean_velocity(sliced_data)

    #Calculate mean's norm'
    norm_mean_velocity = calculate_norm(mean_velocity)

    #Calculate norm of the fluctuation
    calculate_norm_fluctuation(sliced_data, mean_velocity)

    #Calculate fluctuation of the norm
    calculate_fluctuation_norm(sliced_data, norm_mean_velocity)

    #Returns the angles between the mean velocity and each axis
    angle_to_x, angle_to_y, angle_to_z = calculate_angles(mean_velocity, norm_mean_velocity)

    print("This is the norm of the fluctuation")
    print(data['norm_fluctuation'])
    print("This if the fluctuation of the norm")
    print(data['fluctuation_norm'])
    print(f"These are the angles from the mean velocity to; x = {angle_to_x}, y = {angle_to_y}, z = {angle_to_z}")



if __name__ == "__main__":
    main()
