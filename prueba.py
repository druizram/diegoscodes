import numpy as np
import matplotlib.pyplot as plt
from fbm import FBM

# Parámetros del fBm
Hurst_index = 0.7  # Índice de Hurst (0 < H < 1)
length = 1000  # Longitud de la serie temporal
dt = 1  # Paso de tiempo

# Crear objeto fBm
fbm = FBM(n=length, hurst=Hurst_index, length=length, method='daviesharte')

# Generar trayectoria de fBm
fbm_sample = fbm.fbm()

# Visualizar la serie temporal
plt.plot(np.arange(0, length*dt, dt), fbm_sample)
plt.title(f'Fractional Brownian Motion (H = {Hurst_index})')
plt.xlabel('Tiempo')
plt.ylabel('Valor')
plt.show()
