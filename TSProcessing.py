import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf
from sklearn.neighbors import KernelDensity
from scipy.optimize import curve_fit
import statsmodels.api as sm
from scipy.signal import savgol_filter

def read_data(file_path, column_names):
    """Read data from a .asc file and return a Pandas DataFrame."""
    data = pd.read_csv(file_path, parse_dates=['date'], date_format='%Y-%m-%dT%H:%M:%S.%fZ', comment='#', delim_whitespace=True, encoding='ISO-8859-1', names=column_names)
    return data

def plot_velocity(data):
    plt.figure()
    data['u'].plot(label='u velocity', markersize=8, linewidth=1)
    data['v'].plot(label='v velocity', markersize=8, linewidth=1)
    data['w'].plot(label='w velocity', markersize=8, linewidth=1)
    plt.xlabel('hours')
    plt.ylabel('Velocity [m/s]')
    plt.title('Velocity over time')
    plt.legend()
    plt.grid(True)

def plot_speed(data):
    plt.figure()
    data['norm'].plot(label='Mean speed', markersize=8, linewidth=1)
    plt.xlabel('hours')
    plt.ylabel('Speed [m/s]')
    plt.title('Speed over time')
    plt.legend()
    plt.grid(True)


def plot_mean_velocity(data):
    plt.figure()
    data['window_mean_u_velocity'].plot(label='Mean u velocity', markersize=8, linewidth=2)
    data['window_mean_v_velocity'].plot(label='Mean v velocity', markersize=8, linewidth=2)
    data['window_mean_w_velocity'].plot(label='Mean w velocity', markersize=8, linewidth=2)
    plt.xlabel('hours')
    plt.ylabel('Velocity [m/s]')
    plt.title('Mean velocity over time')
    plt.legend()
    plt.grid(True)

def plot_fluctuation_norm(data):
    plt.figure()
    data['fluctuation_norm'].plot(label='Fluctuation of the norm', markersize=8, linewidth=1)
    plt.xlabel('hours')
    plt.ylabel('Adimensional [1]')
    plt.title('Norm and the fluctuation')
    plt.legend()
    plt.grid(True)

def plot_norm_fluctuation(data):
    plt.figure()
    data['norm_fluctuation'].plot(label='Norm fluctuation', markersize=8, linewidth=1)
    plt.xlabel('hours')
    plt.ylabel('Adimensional [1]')
    plt.title('Norm and the fluctuation')
    plt.legend()
    plt.grid(True)

def plot_projection(data):
    plt.figure()
    data['velocity_u_projection'].plot(label='u projection', markersize=8, linewidth=1)
    data['velocity_v_projection'].plot(label='v projection', markersize=8, linewidth=1)
    data['velocity_w_projection'].plot(label='w projection', markersize=8, linewidth=1)
    plt.xlabel('hours')
    plt.ylabel('Velocity [m/s]')
    plt.title('Projection of velocity over mean velocity')
    plt.legend()
    plt.grid(True)


def plot_projection_norm(data):
    plt.figure()
    data['norm_velocity_projection'].plot(label='Norm of projection', markersize=8, linewidth=1)
    plt.xlabel('hours')
    plt.ylabel('Speed [m/s]')
    plt.title('Norm of projection velocity/mean-velocity over time')
    plt.legend()
    plt.grid(True)

def plot_tau_velocity_projection_diff(data):
    plt.figure()
    data['tau_velocity_projection_diff'].plot(label='tau projection diff', markersize=8, linewidth=1)
    plt.xlabel('hours')
    plt.ylabel('Velocity [m/s]')
    plt.title('Tau diff of velocity projection')
    plt.legend()
    plt.grid(True)

def plot_wait_time(data):
    plt.figure()
    data['wait_duration'].plot(label='Ocurrence of gusts', markersize=8, linewidth=1)

    plt.xlabel('hours')
    plt.ylabel('Frequency [1]')
    plt.title('Ocurrence of gusts over time')
    plt.legend()
    plt.grid(True)

def plot_duration(data):
    plt.figure()
    plt.scatter(data.index, data['Duration'], label="Duration of gusts", s=50)
    plt.xlabel('hours')
    plt.ylabel('Duration [s]')
    plt.title('Duration of gusts over time')
    plt.legend()
    plt.grid(True)


def plot_histogram_duration_waiting(data):
    plt.hist(data['wait_duration'], bins=10, edgecolor='black')
    plt.title('Histogram of wait duration')
    plt.xlabel('Wait duration [s]')
    plt.ylabel('Frecuency [1]')

def data_in_interval(data, from_hour, to_hour):
    return data[(data['date'].dt.hour>from_hour) & (data['date'].dt.hour<to_hour)]

def mean_gust_duration_interval(data, from_hour, to_hour):
    return (data[(data['gust_beginning'].dt.hour>from_hour) & (data['gust_end'].dt.hour<to_hour)]['gust_duration'].mean().round(2))

def mean_waiting_duration_interval(data, from_hour, to_hour):
    return (data[(data['wait_beginning'].dt.hour>from_hour) & (data['wait_end'].dt.hour<to_hour)]['wait_duration'].mean().round(2))

def theresNan(arr):
    hay_nan = np.any(np.isnan(arr))
    if hay_nan:
        print("Hay al menos un NaN en el arreglo")
    else:
        print("No hay ningun NaN en el arreglo")

def power_function(x,a):
    return a * x**(-5/3)


def plot_power_spectrum(data, column, min):
    projection_data = data[column].values
    #Quitar los valores nan de projection_data
    #projection_data = projection_data[~np.isnan(projection_data)]
    fft_result = np.fft.fft(projection_data)
    power_spectrum = np.abs(fft_result) ** 2
    # Calculate the corresponding frequencies
    sampling_rate = 10  # 10Hz
    frequencies = np.fft.fftfreq(len(projection_data), d=1/sampling_rate)
    normalized_power_spectrum = power_spectrum / len(data['projection'])
    #Plot de una curva con pendiente loglog -5/3
    expected_amplitude = (1 / frequencies[:len(frequencies)//2]) ** (5/3)
    #Inicio de ajuste
    smoothed_data = savgol_filter(normalized_power_spectrum, window_length=5, polyorder=2)
    #Fin de ajuste
    # Log-Log Plot
    plt.figure()
    plt.loglog(frequencies[:len(frequencies)//2], normalized_power_spectrum[:len(power_spectrum)//2], label='E(f)')
    plt.loglog(frequencies[:len(frequencies)//2], expected_amplitude/10, '--', label='Slope -5/3', color='red')
    #plt.loglog(f_fit, y_fit, label='-5/3 Fitted Slope', color='red')
    plt.title(f'Log-Log Power Spectrum of {column}')
    plt.xlabel('Log(Frequency)')
    plt.ylabel('Log(Velocity)')
    plt.xlim(left=min)
    plt.legend()

def plot_autocorrelation(data, column, from_hour, to_hour):
    sequence = data[column].values
    autocorrelation = sm.tsa.acf(sequence, nlags=len(sequence)-1)
    plt.figure()
    plt.scatter(range(len(autocorrelation)), autocorrelation)
    plt.title(f'Autocorrelation function of {column} between {from_hour}hrs. and {to_hour}hrs. ')
    plt.xlabel('Tau [0.1s]')
    plt.ylabel('Autocorrelation')


def plot_mean_autocorrelation(data, column):
    intervals = [(10, 12), (11, 13), (12, 14), (13, 15), (14, 16)]
    #Calculate autocorrelation for each interval
    autocorrelations = []
    for start_hour, end_hour in intervals:
        interval_data = sm.tsa.acf(data_in_interval(data, start_hour, end_hour)[column].values, nlags=len(data[column].values)-1)
        autocorrelations.append(interval_data)
    # Tratamiento para distintos números de elementos
    max_length = max(len(interval_data) for interval_data in autocorrelations)
    padded_autocorrelations = [np.pad(ac, (0, max_length - len(ac)), constant_values=np.nan) for ac in autocorrelations]
    # Calcula la media ignorando los valores NaN
    mean_autocorrelation = np.nanmean(padded_autocorrelations, axis=0)
    # Grafica la función de autocorrelación
    plt.figure()
    plt.loglog(range(len(mean_autocorrelation)), mean_autocorrelation)
    plt.title(f'Mean autocorrelation function of {column}')
    plt.xlabel('Tau [0.1s]')
    plt.ylabel('Autocorrelation')
    plt.xlim(right=100*10)

def plot_rolling_autocorrelation(data, column, distance_between_beginnings):
    autocorrelations =  []
    for beginning in range(0, len(data), distance_between_beginnings):
        subsequence = data.iloc[beginning: beginning+distance_between_beginnings][column].values
        subsequence_autocorrelation = sm.tsa.acf(subsequence, nlags=len(subsequence)-1)
        autocorrelations.append(subsequence_autocorrelation)
    max_length = max(len(subsequence_autocorrelation) for subsequence_autocorrelation in autocorrelations)
    padded_autocorrelations = [np.pad(ac, (0, max_length - len(ac)), constant_values=np.nan) for ac in autocorrelations]
    #Adicionalmente se obtiene la media y la varianza entre las autocorrelacion para tau 60s, 600s y 6000s
    for tau in [60*10, 600*10]:
        print(f"Mean autocorrelation with tau = {tau}s is {np.mean([arr[tau] for arr in autocorrelations])}")
    for tau in [60*10, 600*10]:
        print(f"Mean autocorrelation with tau = {tau}s is {np.var([arr[tau] for arr in autocorrelations])}")
    #Fin de Adicionalmente
    #Se obtiene un grafico de la varianza acumulada
    #Fin de grafico de varianza acumulada para un tau de 60s
    arr_values = np.array([arr[120*10] for arr in autocorrelations])
    print(f"The number of subsets for autocorrelation particion is {len(arr_values)}")
    varianza_acumulativa = np.cumsum((arr_values - np.mean(arr_values))**2) / np.arange(1, len(arr_values) + 1)
    plt.figure()
    plt.plot(varianza_acumulativa, label='Cumulative variance')
    plt.title('Cumulative Variance by adding more intervals')
    plt.xlabel('Number of intervals considered')
    plt.ylabel('Cumulative variance')
    plt.legend()
     # Calcula la media ignorando los valores NaN
    mean_autocorrelation = np.nanmean(padded_autocorrelations, axis=0)
    # Grafica la función de autocorrelación
    plt.figure()
    plt.scatter(range(len(mean_autocorrelation)), mean_autocorrelation)
    plt.title(f'Mean autocorrelation function of {column} using a window')
    plt.xlabel('Tau [0.1s]')
    plt.ylabel('Autocorrelation')


# Todo: a function that calculates rolling mean given the name of a column to avoid code repetition when calculating mean velocity
def main():
    file_path = 'RealData.asc'  # Name of the file with the Time Series
    column_names = ['date', 'u', 'v', 'w', 'temperature']
    data = read_data(file_path, column_names)
    #Se eliminan las filas que tienen -99.99 de velocidad en cada eje
    data = data[data['u'] != -99.99]

    data['norm'] = np.sqrt(data['u']**2 + data['v']**2 + data['w']**2)
    data['hours'] = data['date'].dt.hour
    # Calculation of squared norm
    data['squared norm'] = data['norm']**2

    window_size = 40*60*10  # 40 minutes window
    # Using the window for calculation of average norm and mean velocity
    data['window_mean_u_velocity'] = data['u'].rolling(window=window_size).mean()
    data['window_mean_v_velocity'] = data['v'].rolling(window=window_size).mean()
    data['window_mean_w_velocity'] = data['w'].rolling(window=window_size).mean()
    data['window_mean_norm'] = data['norm'].rolling(window=window_size).mean()

    # Calculation of fluctuation
    data['u_fluctuation'] = data['u'] - data['window_mean_u_velocity']
    data['v_fluctuation'] = data['v'] - data['window_mean_v_velocity']
    data['w_fluctuation'] = data['w'] - data['window_mean_w_velocity']

    # Calculation of norm fluctuation
    data['fluctuation_norm'] = data['norm'] - data['window_mean_norm']

    # Calculation of fluctuation norm
    data['norm_fluctuation'] = np.sqrt(data['u_fluctuation']**2 + data['v_fluctuation']**2 + data['w_fluctuation']**2)

    # Calculation of mean speed
    data['window_mean_speed'] = np.sqrt(data['window_mean_u_velocity']**2 + data['window_mean_v_velocity']**2 + data['window_mean_w_velocity']**2)

    #Calculation of the projection vector
    vector_velocity = data[['u','v','w']].values
    vector_window_mean_velocity = np.nan_to_num(data[['window_mean_u_velocity', 'window_mean_v_velocity', 'window_mean_w_velocity']].values)
    #Calculas las normas
    norms = np.linalg.norm(vector_window_mean_velocity, axis=1, keepdims=True)
    #Vectores unitarios de la velocidad media
    unitarios = vector_window_mean_velocity / norms
    #Proyecciones de la velocidad sobre la velocidad media (escalar)
    projection = np.sum(vector_velocity * vector_window_mean_velocity, axis=1)[:, np.newaxis] / norms
    data['projection'] = projection
    projection_vector = unitarios * projection
    data['velocity_u_projection'] = projection_vector[:, 0]
    data['velocity_v_projection'] = projection_vector[:, 1]
    data['velocity_w_projection'] = projection_vector[:, 2]

    #Calculo de la fluctuacion de la projection
    data['window_mean_projection'] = data['projection'].rolling(window=window_size).mean()
    data['projection_fluctuation'] = data['projection'] - data['window_mean_projection']

    #Calculation of the norm of the projection vector
    data['norm_velocity_projection'] = np.sqrt(data['velocity_u_projection']**2 + data['velocity_v_projection']**2 + data['velocity_w_projection']**2)

    #Calculo de obtencion de gusts
    #Using forward window for tau diff of projection
    forward_window_size = 3*10
    data['tau_velocity_projection_diff'] = (data['projection'] - data['projection'].shift(-forward_window_size)).abs()
    #Definicion de delta, cota inferior de cambio de velocidad para ser gust
    delta = 0.6

    #Ahora obtengo cada uno de los gusts, su ocurrencia y duracion. La ocurrencia es el tiempo entre dos eventos distintos y la duracion es la duracion de cada uno de los eventos
    data['v_tau_over_delta'] =  data['tau_velocity_projection_diff'] > delta
    #Obtiene cada grupo de gusts
    data['gust'] = (data['v_tau_over_delta'] != data['v_tau_over_delta'].shift(1)).cumsum()
    # Calcular la duración de cada gust
    duration_gust = data[data['v_tau_over_delta']].groupby('gust')['date'].agg(['min', 'max', 'count'])
    duration_gust.columns = ['gust_beginning', 'gust_end', 'gust_duration']
    #Conversion desde decimas de segundo a segundo
    duration_gust['gust_duration'] = duration_gust['gust_duration']/10
    intervals = [(10,12), (11,13), (12,14), (13,15), (14,16)]
    #Imprimimos en pantalla la media de duracion de gust para cada intervalo
    #for (a,b) in intervals:
    #    print(f"The mean gust duration during interval {a}-{b} is: {mean_gust_duration_interval(duration_gust, a, b)}")
    #Se escriben los datos de duration a un txt para mejor visualizacion
    duration_gust.to_csv('duration_gust.txt', index=False, sep='\t')  # sep='\t' indica que el separador es un tabulador

    #De forma analoga, obtiene la duracion de los periodos en que sea menor a delta, o sea los periodos de espera entre gusts
    data['v_tau_under_delta'] = data['tau_velocity_projection_diff'] <= delta
    #Obtiene los grupos de espera entre gusts
    data['waiting'] = (data['v_tau_under_delta'] != data['v_tau_under_delta'].shift(1)).cumsum()
    #Calcula la duracion de cada tiempo de espera
    duration_waiting = data[data['v_tau_under_delta']].groupby('waiting')['date'].agg(['min', 'max', 'count'])
    duration_waiting.columns = ['wait_beginning', 'wait_end', 'wait_duration']
    #Conversion desde tiempo en decimas de segundo a segundo
    duration_waiting['wait_duration'] = duration_waiting['wait_duration']/10
    intervals = [(10,12), (11,13), (12,14), (13,15), (14,16)]
    #Imprimimos en pantalla la media de duracion de waiting para cada intervalo
    #for (a,b) in intervals:
    #    print(f"The mean waiting duration during interval {a}-{b} is: {mean_waiting_duration_interval(duration_waiting, a, b)}")
    #Guardamos los datos de espera entre gusts en un archivo .txt
    duration_waiting.to_csv('duration_waiting.txt', index=False, sep='\t')

    #Ahora se obtiene una aproximacion de la distribucion de probabilidad de duracion de espera entre gusts
    #print(duration_waiting)

    #Ahora se obtiene la aproximacion por kernel de la distribucion de ocurrencia
    """
    datos = duration_waiting['wait_duration'].values.reshape(-1,1)
    kde = KernelDensity(bandwidth=0.5, kernel='gaussian')
    kde.fit(datos)
    x_vals = np.linspace(0, max(datos), 1000).reshape(-1, 1)
    log_density = kde.score_samples(x_vals)
    plt.plot(x_vals, np.exp(log_density), label='PDF Aproximada')
    plt.hist(datos, bins=20, density=True, alpha=0.5, label='Histograma de Datos')
    plt.legend()
    plt.xlabel('Valores')
    plt.ylabel('Densidad de Probabilidad')
    plt.title('Aproximación de la Distribución de Probabilidad')
    """


    #data.set_index('date', inplace=True) #Esto tiene que ir al final, ya que luego no permite acceder a la columna 'date' del dataframe
    #plot_velocity(data)
    #plot_speed(data)
    #plot_mean_velocity(data)
    #plot_norm_fluctuation(data)
    #plot_fluctuation_norm(data)
    #plot_projection(data)
    #plot_projection_norm(data)
    #plot_tau_velocity_projection_diff(data)
    #plot_histogram_duration_waiting(duration_waiting)
    #plot_duration(duration_waiting)
    #plot_acf(duration_waiting['wait_duration'])
    #plot_acf(duration_gust['gust_duration'])
    #plot_acf(data['projection'])
    #duration_waiting['wait_duration'].plot.kde()
    plot_power_spectrum(data_in_interval(data, 10,16), 'projection', 0.0001)
    plot_power_spectrum(data_in_interval(data, 10, 16), 'projection_fluctuation', 0.0001)
    #plot_power_spectrum(data_in_interval(data, 10, 18), 'norm')
    plot_autocorrelation(data_in_interval(data, 10, 12), 'projection', 10, 12)
    plot_mean_autocorrelation(data, 'projection')
    plot_rolling_autocorrelation(data_in_interval(data, 10, 18), 'projection', 40*60*10)
    plt.show()


if __name__ == "__main__":
    main()

# Hay 119 datos que tienen velocidades -99.99, K sugiere borrarlos, comentarselo a M
# Graficar diferencia de norma y fluctuacion

#Puntos
#Se parecen demasiado los power spectrum de la norma de la velocidad en cada instante de tiempo y la projection de la velocidad sobre la velocidad media. Pienso que normalmente hay una componente que es predominante  su valor es casi el mismo que la projection


#Pendientes:
# hacer power law con V'
# obtener autocorrelacion de V para cada uno de los periodos, los 5 y hacer promedio, plotear esto
# PArticionar el intervalo 10 a 14 en subsets de 40 min y sacarles a cada una la autocorrelacion, lugo promediarlos y graficarlos, el inicio de ambos tiene que estar a mas de 40 min
# Calcular la varianza, que deberua reducirse (quizas al principio flucuta, pero despues deberia bajar) (hacer con 3 puntos) (ver xi=60sec)
# Lo que esta prohibido es tomar dos intervalos con menos de 40 min de inicio de ambos
# HAcer lo smismo experimentos de este codigo pero con los 3 otros dias
# Saber que formula usa python para la varianza
